# start from left, generate rand between 0 and i, swap

def knuth_shuffle(arr)
  arr.length.times do |i|
    j = rand(i + 1)
    arr[i], arr[j] = arr[j], arr[i]
  end

  arr
end

arr = [1,2,3,4,5,6,7]

p knuth_shuffle(arr)
