# essentially pre sorts h times before h = 1 (which is then insertion sort)
# this partially sorts the array which means that insertion sort is really fast

def shell_sort(arr)
  arr_len = arr.length
  h = 1
  while h < (arr_len / 3)
    h = (3 * h) + 1
  end

  while h >= 1
    i = 0
    while i < arr_len
      j = i
      
      while j > 0 && arr[j - h] > arr[j]
        if arr[j - h] > arr[j]
          arr[j - h], arr[j] = arr[j], arr[j - h]
        end
        j -= h
      end

      i += h
    end

    h -= 1
  end

  arr
end

arr = [9,9,8,3,2,1,1,0]

p shell_sort(arr)
