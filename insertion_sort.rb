# worst-case O(n^2) when decending order
# best case O(n) when sorted or partially sorted

def insertion_sort(arr)
  arr.length.times do |i|
    j = i

    while j > 0 && arr[j - 1] > arr[j]
      if arr[j - 1] > arr[j]
        arr[j - 1], arr[j] = arr[j], arr[j - 1]
      end
      j -= 1
    end
  end

  arr
end

arr = [9,9,8,3,2,1,1,0]

p insertion_sort(arr)
